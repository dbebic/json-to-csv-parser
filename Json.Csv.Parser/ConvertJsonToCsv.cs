﻿using System.Data;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Json.Csv.Parser
{
    public static class ConvertJsonToCsv
    {
        [FunctionName("convert-json-to-csv")]
        public static async Task<HttpResponseMessage> Run(
           [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequestMessage req,
           ILogger log)
        {
            var json = await req.Content.ReadAsStringAsync();
            var dataTable = ConvertJsonToDataTable(json);
            var csvString = ConvertDataTableToCsv(dataTable);

            var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
            {
                Content = new StringContent(csvString)
            };

            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "convertedJson.csv" };

            return response;
        }

        private static DataTable ConvertJsonToDataTable(string jsonContent)
        {
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(jsonContent);
            return dt;
        }

        private static string ConvertDataTableToCsv(DataTable dataTable)
        {
            StringWriter stringWritter = new StringWriter();
            using (var csv = new CsvWriter(stringWritter))
            {
                csv.Configuration.Delimiter = ",";
                foreach (DataColumn column in dataTable.Columns)
                {
                    csv.WriteField(column.ColumnName);
                }
                csv.NextRecord();

                foreach (DataRow row in dataTable.Rows)
                {
                    for (var i = 0; i < dataTable.Columns.Count; i++)
                    {
                        csv.WriteField(row[i]);
                    }
                    csv.NextRecord();
                }
            }
            return stringWritter.ToString();
        }
    }
}